<?php
date_default_timezone_set("Asia/Ho_Chi_Minh");

$server_name="localhost";

$username="root";

$password="";

$database_name="test";





$gender = array(0 => 'Nam', 1 => 'Nữ');
$faculties = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu", "PY" => "Vật lý");
$nameErr = $genderErr = $facultyErr = $dobErr = $fileErr = "<label></label><br>";
$name_user = $gender_selected = $faculty_selected = $dob_selected = $address_selected = "";

// Check exsits folder
$dir = "upload";
if ( !is_dir( $dir ) ) {
    mkdir( $dir );   
}

if (isset($_POST['submit'])) {
    session_start();
    $_SESSION["name"] = "";
    $_SESSION["gender"] = "";
    $_SESSION["faculty"] = $faculties[$_POST["faculty"]];
    $_SESSION["dob"] = $dob;
    $_SESSION["address"] = $_POST["address"];


    $check_upload = 1;
   

    if (!empty($_FILES["file_upload"]["tmp_name"])) {
        $target_dir = $dir . '/';
        $target_file = $target_dir . basename($_FILES["file_upload"]["name"]);
    
        $image_file_type = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        $origin_file_name = pathinfo($target_file, PATHINFO_FILENAME);
        
        // Rename upload file to {origin name}_YmdHis.{extension}
        $new_file_name = $origin_file_name . '_' . $dob . '.' . $image_file_type;
        $target_file = $target_dir . $new_file_name;
        

        // Check file info
        $check_type = mime_content_type($_FILES["file_upload"]["tmp_name"]);
        $check_size = getimagesize($_FILES["file_upload"]["tmp_name"]);


        // Check if the uploaded file is an image or not
        if ($check_type !== "image/" . $image_file_type) {
            $fileErr = "<label class='error'>Tệp đã chọn không phải tệp hình ảnh.</label><br>";
            $check_upload = 0;
        } else {
            // Allow certain file formats
            if ($image_file_type != "jpg" && $image_file_type != "png" && $image_file_type != "jpeg") {
                $fileErr = "<label class='error'>Chỉ được tải lên tệp jpg, jpeg và png.</label><br>";
                $check_upload = 0;
            } else {
                // Check actual jpg, jpeg, png image or fake extension to upload
                if ($check_size === false) {
                    $fileErr = "<label class='error'>Tệp đã chọn không phải tệp hình ảnh.</label><br>";
                    $check_upload = 0;
                }
            }
        }

        if ($check_upload == 1)    
            move_uploaded_file($_FILES["file_upload"]["tmp_name"], $target_file);
    }

    $_SESSION["file_upload"] = $target_file;


    if (empty($_POST["name"]))
        $nameErr = "<label class='error'>Hãy nhập tên.</label><br>";
    else
        $_SESSION["name"] = $_POST["name"];

    if (empty($_POST["gender"]))
        $genderErr = "<label class='error'>Hãy chọn giới tính.</label><br>";
    else
        $_SESSION["gender"] =  $_POST["gender"];

    $dob = $_POST['dob'];
    
    function isValid($dob, $format = 'd/m/Y'){
        $dt = DateTime::createFromFormat($format, $dob);
        return $dt && $dt->format($format) === $dob;
    }

    if (empty($_POST['dob']))
        $err['dob'] = "<label class='error'>Hãy chọn ngày sinh. <br /></label>";
    else
        if (isValid($dob) == false)
            $err['dob'] = "<label class='error'>Hãy chọn đúng định dạng ngày sinh (d/m/Y).</label><br>";        
        else
            $_SESSION["dob"] = $_POST["dob"];
            
    if ($_SESSION["faculty"] === "")
        $facultyErr = "<label class='error'>Hãy chọn phân khoa.</label><br>";
    else if ($_SESSION["name"] != "" && $_SESSION["gender"] != "" && $_SESSION["dob"] != "" && $check_upload != 0) {
        header('Location: result_register.php');
    }

    try{
        $conn = new PDO("mysql:host=$servername;dbname=test", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $tabe_sql_statement = 
            "CREATE TABLE `student` (
                `id` int(11) NOT NULL,
                `name` varchar(250) NOT NULL,
                `gender` int(1) NOT NULL,
                `faculty` char(3) NOT NULL,
                `birthday` datetime NOT NULL,
                `address` varchar(250) DEFAULT NULL,
                `avartar` text DEFAULT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $conn -> exec($tabe_sql_statement);
    
    
        $name_user = $_SESSION["name"];
        $gender_selected = $_SESSION["gender"];
        $faculty_selected = $_SESSION["faculty"];
        $dob_selected = $_SESSION["dob"];
        $address_selected = $_SESSION["address"];
    
        $sql = $conn-> prepare("INSERT INTO student (name, gender, faculty, birthday, address, avartar) VALUES
        (:name, :gender, :faculty, :birthday, :address)");
        $sql->bindParam(':name',$name_user);
        $sql->bindParam(':gender',$gender_selected);
        $sql->bindParam(':faculty',$faculty_selected);
        $sql->bindParam(':birthday',$dob_selected);
        $sql->bindParam(':address',$address_selected);
        $sql->execute();
    }catch(PDOException $e){
        echo "Connection failed: " . $e->getMessage();
    }


}
?>

<html>
    
    <head>
        <meta charset='UTF-8'>
        <link rel='stylesheet' href='day12.css'>
    </head>

    <body>
        <fieldset>
            <form method='post' action='success_register.php' enctype="multipart/form-data">
            <?php
                echo $nameErr;
                echo $genderErr;
                echo $facultyErr;
                echo $dobErr;
                echo $fileErr;
            ?>

                <table>

                    <tr>
                        <td class='td'><label>Họ và tên<span class="star"> * </span></label></td>
                        <td>
                            <?php 
                            echo "<input type='text' id='input' class='box' name='name' value='";
                            echo isset($_POST['name']) ? $_POST['name'] : '';
                            echo "'>"; ?>
                        </td>
                            
                    </tr>

                    <tr>
                        <td class='td'><label>Giới tính<span class="star"> * </span></label></td>
                        <td>
                        <?php
                            for ($i = 0; $i < count($gender); $i++) {
                                echo
                                    "<input type='radio' name='gender' class='gender' value='" . $gender[$i] . "'";
                                echo (isset($_POST['gender']) && $_POST['gender'] == $gender[$i]) ? " checked " : "";
                                echo "/>" . $gender[$i];
                            }
                        ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class='td'><label>Phân khoa<span class="star"> * </span></label></td>
                        <td>
                            <select class='box' name='faculty'>
                            <?php
                                foreach ($faculties as $key => $value) {
                                    echo "<option";
                                    echo (isset($_POST['faculty']) && $_POST['faculty'] == $key) ? " selected " : "";
                                    echo " value='" . $key . "'>" . $value . "</option>";
                                }
                            ?>  
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td class='td'><label>Ngày sinh<span class="star"> * </span></label></td>
                        <td>
                            <input type='date' name='dob' type='text' data-date-format="DD MMMM YYYY">
                        </td>
                    </tr>

                    <tr>
                        <td class='td'><label>Địa chỉ</label></td>
                        <td>
                            <?php 
                                
                                echo "<input type='text' id='input' class='box' name='address' value='";
                                echo isset($_POST['address']) ? $_POST['address'] : '';
                                echo "'>";
                            ?>
                                
                        </td>
                    </tr>

                    <tr>
                        <td class='td'><label>Hình ảnh</label></td>
                        <td>
                            <input type='file' id='input' name='file_upload' >
  
                        </td>
                    </tr>

                </table>

                <button name='submit' type='submit'>Đăng ký</button>
                
            </form>

        </fieldset>

    </body>

</html>
